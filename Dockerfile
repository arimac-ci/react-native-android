FROM reactnativecommunity/react-native-android

RUN useradd --create-home react
RUN chown -R react:react /opt/android

USER react
